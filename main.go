package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
)

func main() {
	app := fiber.New()

	app.Get("/sendnotif", sendNotif)

	log.Fatal(app.Listen(":3000"))
}

func sendNotif(c *fiber.Ctx) error {
	oneSigUrl := "https://onesignal.com/api/v1/notifications"

	req := fasthttp.AcquireRequest()

	// Set Header Request
	fmt.Println("--> Initialize Header Request")
	req.Header.SetMethod("POST")
	req.Header.SetContentType("application/json")
	req.Header.Set("Authorization", "Basic ZjEzOWZlZWYtMTU1Ny00YTVlLWFjY2MtNTc1NmZhZTEyZGFh")
	req.SetRequestURI(oneSigUrl)

	// Set Body Request
	fmt.Println("--> Initialize Body Request")
	reqBody := struct {
		AppID            string   `json:"app_id"`
		IncludedSegments []string `json:"included_segments"`
		Contents         struct {
			En string `json:"en"`
		} `json:"contents"`
	}{
		AppID:            "ec95fd08-600b-498d-a2c6-5dddc88e095a",
		IncludedSegments: []string{
			"Subscribed Users",
		},
		Contents: struct {
			En string "json:\"en\""
		}{
			"Selamat Malam Tegar",
		},
	}
	body, _ := json.Marshal(reqBody)
	req.SetBody(body)

	res := fasthttp.AcquireResponse()
	if err := fasthttp.Do(req, res); err != nil {
		panic("handle error")
	}
	fasthttp.ReleaseRequest(req)

	// body := res.Body()
	// Do something with body.

	fasthttp.ReleaseResponse(res) // Only when you are done with body!

	return c.JSON("success")
}